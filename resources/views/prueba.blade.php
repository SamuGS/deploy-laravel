<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}"> 
</head>
<body>
    <p>@lang("main.hello")</p>
    <img src="{{asset('images/error404.jpg')}}" alt="Error 404">
</body>
</html>