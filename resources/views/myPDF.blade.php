<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
</head>
<body>
    <h1>Clientes mayores de edad</h1>
    <table>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Fecha nacimiento</th>
            <th>Imagen</th>
        </tr>

        @foreach( $clientes as $key => $cliente )
        <tr>
            <td>{{$cliente->nombre}}</td>
            <td>{{$cliente->correo}}</td>
            <td>{{$cliente->fecha_nacimiento}}</td>
            <td><img src="{{$cliente->imagen}}" alt="" style="height:200px" ></td>
        </tr>
        @endforeach

    </table>
</body>
</html>