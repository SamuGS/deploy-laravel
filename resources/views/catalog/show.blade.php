@extends('layouts.master')

@section('content')

@include('catalog.destroy')

<div class="row">

    <div class="col-sm-4">

        <img src="{{$cliente->imagen}}" style="height:200px" />

    </div>

    <div class="col-sm-8">

        <h4 style="min-height:45px;margin:5px 0 10px 0">

            {{$cliente->nombre}}

        </h4>

        <h4 style="min-height:45px;margin:5px 0 10px 0">

            {{$cliente->fecha_nacimiento}}

        </h4>

        <h4 style="min-height:45px;margin:5px 0 10px 0">

            {{$cliente->correo}}

        </h4>

        <a href="/catalog/edit/{{$cliente->id}}" class="btn btn-warning">Editar</a>

        <!-- <form method="POST" action="{{url('/catalog/delete').'/'.$cliente->id}}" style="display:inline">

            @method('DELETE')

            @csrf

            <button type="submit" class="btn btn-danger" role="button">

                Borrar

            </button>

        </form> -->

        <a data-target="#modal-delete-{{$cliente->id}}" data-toggle="modal" class="btn btn-danger ml-2">Eliminar</a>

        <a href="/catalog/" class="btn btn-primary">Volver</a>

    </div>

</div>

@stop