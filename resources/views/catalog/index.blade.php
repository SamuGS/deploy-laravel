@extends('layouts.master')
@section('content')

<div class="row">

    @if(Session::has('correcto'))

    <div class="alert alert-success"> {{ Session::get('correcto') }}</div>

    @endif

    @foreach( $clientes as $key => $cliente )

    <div class="col-xs-6 col-sm-4 col-md-3 text-center">

        <a href="{{ url('/catalog/show/' . $cliente->id) }}">

            <img src="{{$cliente->imagen}}" style="height:200px" />

            <h4 style="min-height:45px;margin:5px 0 10px 0">

                {{$cliente->nombre}}

            </h4>

        </a>

    </div>

    @endforeach

</div>

{{ $clientes->links() }}

@stop