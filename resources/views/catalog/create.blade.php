@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            @if ($errors->any())

            <div class="row justify-content-center">

                <div class="col-sm-7">

                    <div class="alert alert-danger">

                        <ul>

                            @foreach($errors->all() as $error)

                            <li>{{$error}}</li>

                            @endforeach

                        </ul>

                    </div>

                </div>

            </div>

            @endif

            <div class="card-header text-center">
                Añadir cliente
            </div>
            <div class="card-body" style="padding:30px">

                <form method="POST" action="" enctype="multipart/form-data">

                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
                    </div>

                    <div class="form-group mt-3">
                        <label for="imagen">Imagen:</label>

                        <input id="imagen" name="imagen" type="file">
                    </div>

                    <div class="form-group mt-3">
                        <label for="fecha_nacimiento">Fecha nacimiento:</label>

                        <input id="fecha_nacimiento" value="{{old('fecha_nacimiento')}}" name="fecha_nacimiento" type="date">
                    </div>

                    <div class="form-group mt-3">
                        <label for="correo">Email:</label>
                        <input id="correo" name="correo" value="{{old('correo')}}" type="email">
                    </div>

                    <div class="form-group mt-3 text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir cliente
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@stop