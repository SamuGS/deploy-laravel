@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
        @if ($errors->any())

<div class="row justify-content-center">

    <div class="col-sm-7">

        <div class="alert alert-danger">

            <ul>

                @foreach($errors->all() as $error)

                <li>{{$error}}</li>

                @endforeach

            </ul>

        </div>

    </div>

</div>

@endif
            <div class="card-header text-center">
                Modificar cliente
            </div>
            <div class="card-body" style="padding:30px">

                <form action="{{url('catalog/edit/'.$cliente->id)}}" method="POST" enctype="multipart/form-data">

                    @method('PUT')

                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" id="name" value="{{$cliente->nombre}}" class="form-control">
                    </div>

                    <div class="form-group mt-3">
                        <label for="imagen">Imagen:</label>

                        <input id="imagen" name="imagen" type="file">
                    </div>

                    <div class="form-group mt-3">
                        <label for="fecha_nacimiento">Fecha nacimiento:</label>

                        <input id="fecha_nacimiento" name="fecha_nacimiento" type="date" value="{{$cliente->fecha_nacimiento}}">
                    </div>

                    <div class="form-group mt-3">
                        <label for="correo">Correo:</label>
                        <input id="correo" name="correo" type="email" value="{{$cliente->correo}}">
                    </div>

                    <div class="form-group mt-3 text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Modificar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@stop