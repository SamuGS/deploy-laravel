<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // private $arrayClientes = array(

    //     array(

    //         'nombre' => 'samu',

    //         'imagen' => 'https://image.freepik.com/vector-gratis/icono-hombre-persona-pensando_24877-40527.jpg',

    //         'fecha_nacimiento' => '2014-01-08',

    //         'correo' => 'samu@samu.es'

    //     ),

    //     array(

    //         'nombre' => 'Ruby',

    //         'imagen' => 'https://concepto.de/wp-content/uploads/2018/08/persona-e1533759204552.jpg',

    //         'fecha_nacimiento' => '1997-01-08',

    //         'correo' => 'ruby@ruby.es'

    //     )

    // );

    public function run()
    {
        // \DB::table("clientes")->insert($this->arrayClientes);
        // Cliente::create(['nombre'=>'Pedro', 'fecha_nacimiento'=>'2001-01-05', 'correo'=>'pedro@arribi.es']);
        \DB::table('clientes')->delete();
        // Cliente::create(['nombre'=>'Jesus', 'fecha_nacimiento'=>'2000-01-05', 'correo'=>'arribi@arribi.es']);
        Cliente::factory()->count(30)->create();
    }
}
