<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MailController extends Controller
{
    public function html_email()
    {
        $data = array('name' => "Virat Gandhi");
        Mail::send('pruebaEmail', $data, function ($message) {
            $message->to('samuelgomezstunt@gmail.com', 'Cliente')->subject('Realizando pruebas');
            $message->from('samugstunt@gmail.com', 'Samuel');
        });
        echo "HTML Email Sent. Check your inbox.";
    }

    public function felicita(){

        $arrayClientes = DB::table('clientes')

            ->whereMonth('fecha_nacimiento', date('n'))

            ->whereDay('fecha_nacimiento', date('j'))->get();

        foreach ($arrayClientes as $cliente) {

            $datosCliente = array('name' => $cliente->nombre);

            Mail::send('pruebaEmail', $datosCliente, function ($message) use ($cliente) {

                $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));

                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_USERNAME'));

                $message->attach(asset($cliente->imagen));
            });
        }

        echo "Email Sent with congratulations! Check your inbox.";
    }
}
