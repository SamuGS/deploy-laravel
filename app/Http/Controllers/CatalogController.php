<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cliente;

use Illuminate\Support\Facades\Validator;

use App\Http\Requests\ClienteFormRequest;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;


class CatalogController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
         $this->middleware('permission:product-create', ['only' => ['create','store']]);
         $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    public function getIndex(Request $request)
    {

        $texto = trim($request->get('texto')); //Lo cogemos por URL (método GET)

        $clientes = DB::table('clientes')

            ->select()

            ->where('nombre', 'LIKE', '%' . $texto . '%')->paginate(10);

        return view('catalog/index', compact('clientes', 'texto'));
    }

    public function getShow($id)
    {
        $nombre = Auth::user()->name;
        $cliente = Cliente::findOrFail($id);
        return view('catalog/show');
    }

    public function getEdit($id)
    {
        $nombre = Auth::user()->name;
        $cliente = Cliente::findOrFail($id);
        return view('catalog/edit', array('cliente' => $cliente, 'nombre' => $nombre));
    }

    public function getCreate()
    {
        $nombre = Auth::user()->name;
        return view("catalog/create", compact('nombre'));
    }

    public function postCreate(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'imagen' => 'nullable|file',
            'fecha_nacimiento' => 'required|date',
            'correo' => 'email:rfc'
        ]);

        if ($validator->fails()) {
            return redirect('catalog/create')
                ->withErrors($validator)
                ->withInput();
        }
        $cliente = new Cliente();
        $cliente->nombre = $request->input('name');
        if ($request->file('imagen') !== null) {
            $cliente->imagen = asset('storage/' . $request->file('imagen')->hashName());

            $request->validate([

                'imagen' => 'required|mimes:png,jpg|max:2048',

            ]);

            $request->file('imagen')->store('public');
        }

        $cliente->fecha_nacimiento = $request->input('fecha_nacimiento');

        $cliente->correo = $request->input('correo');

        $cliente->save();

        $request->session()->flash('correcto', 'Se ha creado el registro.');

        return redirect("catalog");
    }

    public function putEdit(ClienteFormRequest $request, $id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->nombre = $request->input('name');
        if ($request->file('imagen') !== null) {
            $cliente->imagen = asset('storage/' . $request->file('imagen')->hashName());

            $request->validate([

                'imagen' => 'required|mimes:png,jpg|max:2048',

            ]);

            $request->file('imagen')->store('public');
        }

        $cliente->fecha_nacimiento = $request->input('fecha_nacimiento');

        $cliente->correo = $request->input('correo');

        $cliente->save();

        $request->session()->flash('correctoEdit', 'Se ha actualizado el registro con éxito.');

        return view("catalog/show", array('cliente' => $cliente));
    }

    public function delete($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();

        return redirect("catalog");
    }
}
