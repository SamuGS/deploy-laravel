<?php

namespace App\Http\Controllers;

use App\Models\Models\PruebaModel;
use Illuminate\Http\Request;

class Prueba2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\PruebaModel  $pruebaModel
     * @return \Illuminate\Http\Response
     */
    public function show(PruebaModel $pruebaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\PruebaModel  $pruebaModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PruebaModel $pruebaModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\PruebaModel  $pruebaModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PruebaModel $pruebaModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\PruebaModel  $pruebaModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PruebaModel $pruebaModel)
    {
        //
    }
}
