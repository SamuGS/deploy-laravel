<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\Cliente;

use PDF;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF()
    {
        $arrayClientes = Cliente::all();

        foreach ($arrayClientes as $cliente) {
            $fecha = date("d-m-Y", strtotime($cliente->fecha_nacimiento . "+ 18 year"));
            if ($fecha <= date("d-m-Y")) {
                $clientes[] = $cliente;
            }
        }

        $data = [
            'clientes' => $clientes,
        ];

        $pdf = PDF::loadView('myPDF', $data);
        return $pdf->download('itsolutionstuff.pdf');
    }
}
