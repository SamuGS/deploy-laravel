<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'imagen' => 'nullable|file',
            'fecha_nacimiento' => 'required|date|before_or_equal:today',
            'correo' => 'required|email:rfc',
        ];
    }

    public function messages()
    {
        return [
                'fecha_nacimiento.before_or_equal' => 'Aún no has nacido'
            ];
    }
}
