<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Mail;

use Illuminate\Support\Facades\DB;

class EnviarCorreo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envia:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para enviar correo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arrayClientes = DB::table('clientes')

            ->whereMonth('fecha_nacimiento', date('n'))

            ->whereDay('fecha_nacimiento', date('j'))->get();

        foreach ($arrayClientes as $cliente) {

            $datosCliente = array('name' => $cliente->nombre);

            Mail::send('pruebaEmail', $datosCliente, function ($message) use ($cliente) {

                $message->to($cliente->correo, $cliente->nombre)->subject(env('APP_NAME'));

                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_USERNAME'));

                $message->attach(asset($cliente->imagen));
            });
        }

        echo "Email Sent with congratulations! Check your inbox.";
        return 0;
    }
}
