<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\NombreControlador;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\MyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);


// Route::view('/', 'home')->middleware('language');

Route::resource ('/request', PruebaController::class);

Route::get('/auth/login/', function () {
    return view('auth/login');
});

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => ['verified','auth']], function () {

    Route::get('/catalog', [CatalogController::class, 'getIndex']);
    
    Route::get('/catalog/show/{id}', [CatalogController::class, 'getShow']);
    
    Route::get('/catalog/edit/{id}', [CatalogController::class, 'getEdit']);

    Route::put('/catalog/edit/{id}', [CatalogController::class, 'putEdit']);
    
    Route::get('/catalog/create', [CatalogController::class, 'getCreate']);

    Route::post('/catalog/create', [CatalogController::class, 'postCreate']);

    Route::delete('/catalog/delete/{id}', [CatalogController::class, 'delete']);

});

Route::get('/catalog', [CatalogController::class, 'getIndex']);

Route::get('/catalog/delete/{id}', function ($id = null) {
    return "Elimina los datos del cliente $id";
})->middleware('auth');

Route::resource ('/prueba', NombreControlador::class);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/sendhtmlemail', [MailController::class, 'html_email']);
Route::get('/felicita', [MailController::class, 'felicita']);
// Route::get('/felicita', [PDFController::class, 'felicita']);


// PDF
Route::get('generate-pdf', [PDFController::class, 'generatePDF']);

// EXCEL
Route::get('importExportView', [MyController::class, 'importExportView']);
Route::get('export', [MyController::class, 'export'])->name('export');
Route::post('import', [MyController::class, 'import'])->name('import');

// ROLES
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
});